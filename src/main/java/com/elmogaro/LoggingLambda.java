package com.elmogaro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class LoggingLambda {
    private static final Logger LOG = LoggerFactory.getLogger(LoggingLambda.class);

    public void handler(String s) {
        RuntimeMXBean mxbean = ManagementFactory.getRuntimeMXBean();

        LOG.info("jvm name = [{}]", mxbean.getVmName());
        LOG.info("jvm vendor = [{}]", mxbean.getVmVendor());
        LOG.info("jvm version = [{}]", mxbean.getVmVersion());

        LOG.info("jvm free memory = [{}]", Runtime.getRuntime().freeMemory());
        LOG.info("jvm max memory = [{}]", Runtime.getRuntime().maxMemory());
        LOG.info("jvm total memory = [{}]", Runtime.getRuntime().totalMemory());

        LOG.info("jvm available processors = [{}]", Runtime.getRuntime().availableProcessors());

        mxbean.getInputArguments()
                .forEach(i -> LOG.info("jvm input argument = [{}]", i));

        LOG.info("jvm boot classpath = [{}]", mxbean.getBootClassPath());
        LOG.info("jvm classpath = [{}]", mxbean.getClassPath());

        mxbean.getSystemProperties()
                .forEach((k, v) -> LOG.info("jvm system property = [{} => {}]", k, v));
    }
}